/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package app.streetmunch;

public final class R {
    public static final class attr {
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int chipAlternatesLayout=0x7f010004;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int chipBackground=0x7f010001;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int chipBackgroundPressed=0x7f010002;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int chipDelete=0x7f010003;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int chipFontSize=0x7f010007;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int chipHeight=0x7f010006;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int chipPadding=0x7f010005;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int invalidChipBackground=0x7f010000;
    }
    public static final class color {
        public static final int default_screen_bg=0x7f080000;
        public static final int default_text_color=0x7f080003;
        public static final int rounded_container_bg=0x7f080001;
        public static final int rounded_container_border=0x7f080002;
        public static final int title_bg=0x7f080004;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f040004;
        public static final int activity_vertical_margin=0x7f040005;
        public static final int chip_height=0x7f040001;
        public static final int chip_padding=0x7f040000;
        public static final int chip_text_size=0x7f040002;
        public static final int line_spacing_extra=0x7f040003;
    }
    public static final class drawable {
        public static final int background_view_rounded_single=0x7f020000;
        public static final int border=0x7f020001;
        public static final int btn_circle_selected=0x7f020002;
        public static final int camera=0x7f020003;
        public static final int chip_background=0x7f020004;
        public static final int chip_background_invalid=0x7f020005;
        public static final int chip_background_selected=0x7f020006;
        public static final int chip_delete=0x7f020007;
        public static final int cry=0x7f020008;
        public static final int custom_thumb_state_default=0x7f020009;
        public static final int custom_thumb_state_selected=0x7f02000a;
        public static final int custombutton=0x7f02000b;
        public static final int grid_normal=0x7f02000c;
        public static final int grid_selected=0x7f02000d;
        public static final int grid_selector=0x7f02000e;
        public static final int happy=0x7f02000f;
        public static final int ic_contact_picture=0x7f020010;
        public static final int ic_launcher=0x7f020011;
        public static final int launcher_icon=0x7f020012;
        public static final int list_item_font_primary=0x7f020013;
        public static final int list_item_font_secondary=0x7f020014;
        public static final int logo=0x7f020015;
        public static final int photo_bg=0x7f020016;
        public static final int postbutton=0x7f020017;
        public static final int progressbar=0x7f020018;
        public static final int ratings_thumb=0x7f020019;
    }
    public static final class id {
        public static final int action_settings=0x7f0a000b;
        public static final int businessPhoto=0x7f0a0007;
        public static final int food_item_text=0x7f0a0001;
        public static final int header=0x7f0a000a;
        public static final int pictureHint=0x7f0a0006;
        public static final int ratingLabel=0x7f0a0003;
        public static final int ratingLayout=0x7f0a0002;
        public static final int ratingsBar=0x7f0a0005;
        public static final int reviewText=0x7f0a0008;
        public static final int slider=0x7f0a0004;
        public static final int submitBtn=0x7f0a0009;
        public static final int text1=0x7f0a0000;
    }
    public static final class integer {
        public static final int chips_max_lines=0x7f050000;
    }
    public static final class layout {
        public static final int chips_alternate_item=0x7f030000;
        public static final int chips_recipient_dropdown_item=0x7f030001;
        public static final int copy_chip_dialog_layout=0x7f030002;
        public static final int more_item=0x7f030003;
        public static final int submit_activity=0x7f030004;
        public static final int submit_header=0x7f030005;
    }
    public static final class menu {
        public static final int main=0x7f090000;
    }
    public static final class string {
        public static final int action_settings=0x7f060005;
        public static final int app_name=0x7f060004;
        public static final int business_review=0x7f06000a;
        public static final int button_submit=0x7f060008;
        public static final int category_label=0x7f060006;
        /**  Text displayed when the user long presses on a chip to copy the recipients email address.
         [CHAR LIMIT=200] 
         */
        public static final int copy_email=0x7f060001;
        /**  Text displayed when the user long presses on a chip to copy the recipient's phone number.
         [CHAR LIMIT=200] 
         */
        public static final int copy_number=0x7f060002;
        /**  Text displayed in the enter key slot when the recipientedittextview has focus.
         [CHAR LIMIT=12] 
         */
        public static final int done=0x7f060003;
        public static final int food_item_hint=0x7f06000b;
        /**  Text displayed when the recipientedittextview is not focused. Displays the total number of recipients since the field is shrunk to just display a portion 
         */
        public static final int more_string=0x7f060000;
        public static final int picture_label=0x7f060009;
        public static final int ratings_label=0x7f060007;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.












    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.












        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070003;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070004;
        public static final int CustomTheme=0x7f070002;
        public static final int CustomWindowTitleBackground=0x7f070001;
        public static final int RecipientEditTextView=0x7f070000;
        public static final int boldText=0x7f070006;
        public static final int customBarStyle=0x7f070005;
        public static final int normalText=0x7f070007;
    }
    public static final class styleable {
        /** Attributes that can be used with a RecipientEditTextView.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #RecipientEditTextView_chipAlternatesLayout app.streetmunch:chipAlternatesLayout}</code></td><td></td></tr>
           <tr><td><code>{@link #RecipientEditTextView_chipBackground app.streetmunch:chipBackground}</code></td><td></td></tr>
           <tr><td><code>{@link #RecipientEditTextView_chipBackgroundPressed app.streetmunch:chipBackgroundPressed}</code></td><td></td></tr>
           <tr><td><code>{@link #RecipientEditTextView_chipDelete app.streetmunch:chipDelete}</code></td><td></td></tr>
           <tr><td><code>{@link #RecipientEditTextView_chipFontSize app.streetmunch:chipFontSize}</code></td><td></td></tr>
           <tr><td><code>{@link #RecipientEditTextView_chipHeight app.streetmunch:chipHeight}</code></td><td></td></tr>
           <tr><td><code>{@link #RecipientEditTextView_chipPadding app.streetmunch:chipPadding}</code></td><td></td></tr>
           <tr><td><code>{@link #RecipientEditTextView_invalidChipBackground app.streetmunch:invalidChipBackground}</code></td><td></td></tr>
           </table>
           @see #RecipientEditTextView_chipAlternatesLayout
           @see #RecipientEditTextView_chipBackground
           @see #RecipientEditTextView_chipBackgroundPressed
           @see #RecipientEditTextView_chipDelete
           @see #RecipientEditTextView_chipFontSize
           @see #RecipientEditTextView_chipHeight
           @see #RecipientEditTextView_chipPadding
           @see #RecipientEditTextView_invalidChipBackground
         */
        public static final int[] RecipientEditTextView = {
            0x7f010000, 0x7f010001, 0x7f010002, 0x7f010003,
            0x7f010004, 0x7f010005, 0x7f010006, 0x7f010007
        };
        /**
          <p>This symbol is the offset where the {@link app.streetmunch.R.attr#chipAlternatesLayout}
          attribute's value can be found in the {@link #RecipientEditTextView} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name app.streetmunch:chipAlternatesLayout
        */
        public static final int RecipientEditTextView_chipAlternatesLayout = 4;
        /**
          <p>This symbol is the offset where the {@link app.streetmunch.R.attr#chipBackground}
          attribute's value can be found in the {@link #RecipientEditTextView} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name app.streetmunch:chipBackground
        */
        public static final int RecipientEditTextView_chipBackground = 1;
        /**
          <p>This symbol is the offset where the {@link app.streetmunch.R.attr#chipBackgroundPressed}
          attribute's value can be found in the {@link #RecipientEditTextView} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name app.streetmunch:chipBackgroundPressed
        */
        public static final int RecipientEditTextView_chipBackgroundPressed = 2;
        /**
          <p>This symbol is the offset where the {@link app.streetmunch.R.attr#chipDelete}
          attribute's value can be found in the {@link #RecipientEditTextView} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name app.streetmunch:chipDelete
        */
        public static final int RecipientEditTextView_chipDelete = 3;
        /**
          <p>This symbol is the offset where the {@link app.streetmunch.R.attr#chipFontSize}
          attribute's value can be found in the {@link #RecipientEditTextView} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name app.streetmunch:chipFontSize
        */
        public static final int RecipientEditTextView_chipFontSize = 7;
        /**
          <p>This symbol is the offset where the {@link app.streetmunch.R.attr#chipHeight}
          attribute's value can be found in the {@link #RecipientEditTextView} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name app.streetmunch:chipHeight
        */
        public static final int RecipientEditTextView_chipHeight = 6;
        /**
          <p>This symbol is the offset where the {@link app.streetmunch.R.attr#chipPadding}
          attribute's value can be found in the {@link #RecipientEditTextView} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name app.streetmunch:chipPadding
        */
        public static final int RecipientEditTextView_chipPadding = 5;
        /**
          <p>This symbol is the offset where the {@link app.streetmunch.R.attr#invalidChipBackground}
          attribute's value can be found in the {@link #RecipientEditTextView} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name app.streetmunch:invalidChipBackground
        */
        public static final int RecipientEditTextView_invalidChipBackground = 0;
    };
}
