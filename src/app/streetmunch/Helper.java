package app.streetmunch;

import java.util.ArrayList;
import java.util.List;

import com.android.ex.chips.RecipientEntry;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Helper class with utility methods
 * master branch changes
 * @author Chandra Ramachandran 
 * CHECKING GIT 
 * changes to master!!
 */
public class Helper {

	/**
	 * Constructs List of food entries that will be used for auto complete. TODO
	 * - Use a Parse to fetch the entry. Currently it is hardcoded
	 * 
	 * @return List of RecipientEntry objects
	 */
	public static List<RecipientEntry> constructFoodEntries() {
		final List<RecipientEntry> entries = new ArrayList<RecipientEntry>();

		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
				"foodCategories");
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> foodCategories, ParseException e) {
				if (e == null) {
					for (ParseObject foodCategory : foodCategories) {
						String categoryName = (String) foodCategory
								.get("foodCategoryName");
						entries.add(RecipientEntry.constructGeneratedEntry(
								categoryName, categoryName, true));

					}
				} else {
					// TODO - Handle Error
				}
			}
		});

		return entries;
	}

}
