package app.streetmunch;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import sree.set; amster
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import com.android.ex.chips.BaseRecipientAdapter;
import com.android.ex.chips.RecipientEditTextView;
import com.android.ex.chips.RecipientEntry;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

/**
 * Main activity for the app. This activity renders the view where the users can
 * post their review and submit photos of the business
 * 
 * @author Chandra Ramachandran
 * 
 */
public class SubmitActivity extends Activity {

	/** Parse Application Id **/
	private static final String PARSE_APPLICATION_ID = "oxkyrLF9vuoetQOX9HpdMRilkIBwDkjLZ30VHjPR";

	/** Parse Client Key **/
	private static final String PARSE_CLIENT_KEY = "WgSyLIQJvkAs3MuyobjOFczW9fq0peT3jg8VQFHU";

	/** Instance of Android Location **/
	private Location thisLocation;

	/** Instance of current Android Context **/
	private Context thisContext;

	/** URI path to the image location **/
	private Uri imageUri;

	/** Android Location Manager instance **/
	private LocationManager locationManager;

	/**
	 * Instantiates and creates the view
	 * 
	 * @param savedInstanceState
	 * sree
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// Initialize Parse
		Parse.initialize(this, PARSE_APPLICATION_ID, PARSE_CLIENT_KEY);

		// Get the current context;
		thisContext = getApplicationContext();

		// Inform Android that we are customizing the header title bar
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

		// Set the view
		setContentView(R.layout.submit_activity);

		// Initialize the header bar
		initTitleBar();

		// Initialize the location
		initLocation();

		// Initializes the food auto-complete text box
		initFoodSelector();

		// Initialize the ratings seek bar
		initRatingsBar();
	}

	/**
	 * Initialize the location using GPS Location manager. Sets the location in
	 * the class level variable to be used later. If GPS no provider can be
	 * found, then it shows a message to the user
	 */
	private void initLocation() {

		locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);

		// If GPS Provider is not enabled, then alert the user
		if (!isGPSEnabled()) {
			buildAlertMessageNoGps();
		}

		// Get the last known location
		thisLocation = locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		if (thisLocation == null) {
			Toast.makeText(thisContext,
					"Unable to determine your current location. Trying again",
					Toast.LENGTH_LONG).show();
		}

		// Define a listener that responds to location updates
		LocationListener locationListener = new LocationListener() {
			public void onLocationChanged(Location location) {
				// Called when a new location is found by the network location
				// provider.
				// makeUseOfNewLocation(location);
				if (thisLocation == null) {
					Toast.makeText(
							thisContext,
							"Obtained location: Lat: " + location.getLatitude()
									+ ", Long: " + location.getLongitude(),
							Toast.LENGTH_LONG).show();
				}

				thisLocation = location;
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}
		};

		// Register the listener with the Location Manager to receive location
		// updates
		if (isGPSEnabled()) {
			locationManager.requestLocationUpdates(
					LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		}
	}

	/**
	 * Initializes the food selector text box. Uses the Android Chips library to
	 * show the selections as chips.
	 */
	private void initFoodSelector() {
		final RecipientEditTextView customRetv = (RecipientEditTextView) findViewById(R.id.food_item_text);
		customRetv.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
		customRetv.setAdapter(new BaseRecipientAdapter(thisContext, 3, Helper
				.constructFoodEntries()) {
		});
	}

	/**
	 * Initializes the header title bar. This title bar contains the logo and
	 * text
	 */
	private void initTitleBar() {
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.submit_header);
		getWindow().getDecorView().setBackgroundColor(
				getResources().getColor(R.color.default_screen_bg));

	}

	/**
	 * Handles the event when the user clicks on "take a picture" Starts the
	 * ACTION_IMAGE_CAPTURE intent
	 * 
	 * @param view
	 */
	public void takePhoto(View view) {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		// Save the file with a specified name
		imageUri = Uri.fromFile(new File(Environment
				.getExternalStorageDirectory(), "businessPhoto.jpg"));

		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

		// Trigger intent
		startActivityForResult(intent, 1);
	}

	/**
	 * Used to capture the result of photo capture
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case 1: // Photo Capture
			if (resultCode == Activity.RESULT_OK) {

				// Get the handle of the image path
				Uri selectedImage = imageUri;
				getContentResolver().notifyChange(selectedImage, null);

				// Populate the image view with the latest photo taken
				ImageView imageView = (ImageView) findViewById(R.id.businessPhoto);
				imageView.setVisibility(ImageView.VISIBLE);
				imageView.setTag(selectedImage.getPath());

				ContentResolver cr = getContentResolver();
				Bitmap bitmap;
				try {
					// Update image in view
					bitmap = android.provider.MediaStore.Images.Media
							.getBitmap(cr, selectedImage);

					imageView.setImageBitmap(bitmap);
				} catch (Exception e) {
					Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT)
							.show();
					e.printStackTrace();
					Log.e("Camera", e.toString());
				}
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this,
						"Picture was not taken. User cancelled the action.",
						Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this,
						"Picture was not taken due to an unknown reason",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	/**
	 * Checks if GPS Provider is enabled for the device
	 * 
	 * @return true if GPS is enabled
	 */
	private boolean isGPSEnabled() {
		boolean gpsEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		return gpsEnabled;
	}

	/**
	 * Builds the alert which Prompts the user to enable location
	 */
	private void buildAlertMessageNoGps() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(
				"Your GPS seems to be disabled. GPS needs to be enabled to use this application. Do you want to enable it?")
				.setCancelable(false)
				.setPositiveButton("Yes", new OnClickListener() {
					public void onClick(final DialogInterface dialog,
							final int id) {
						startActivity(new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS));
					}
				}).setNegativeButton("No", new OnClickListener() {
					public void onClick(final DialogInterface dialog,
							final int id) {
						dialog.cancel();
						Toast.makeText(
								thisContext,
								"You will not be able to use this application without enabling location. Please check your settings and try again",
								Toast.LENGTH_LONG).show();
					}
				});

		final AlertDialog alert = builder.create();
		alert.show();
	}

	/**
	 * Initializes the ratings seek bar with the default values.
	 */
	private void initRatingsBar() {

		final SeekBar sk = (SeekBar) findViewById(R.id.ratingsBar);
		sk.setProgress(50);
		sk.setTag(-1);
		sk.setMax(100);
		sk.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if (fromUser) {
					seekBar.setTag(progress);
				}
			}
		});

	}

	/**
	 * Handles the form submit. First it validates if the location is enabled,
	 * we want to get the latest location during submit. Then it checks if
	 * ratings and photo are provided. If any of the validations fail, it shows
	 * an alert box to the user.
	 * 
	 * @param view
	 */
	public void postValues(View view) {

		// If GPS is not enabled, then show the message.
		if (!isGPSEnabled()) {
			buildAlertMessageNoGps();
		}

		int rating = -1;
		String imagePath = "";
		String foodCategoriesStr = "";
		List<String> foodCategoriesArray = new ArrayList<String>();

		// Get the categories
		RecipientEditTextView foodItemTextView = (RecipientEditTextView) findViewById(R.id.food_item_text);
		foodCategoriesStr = foodItemTextView.getText().toString().trim();
		Set<RecipientEntry> recipients = foodItemTextView.getChosenRecipients();

		for (RecipientEntry recipient : recipients) {
			foodCategoriesArray.add(recipient.getDisplayName());
		}

		// Get the ratings
		SeekBar ratingsBar = (SeekBar) findViewById(R.id.ratingsBar);
		rating = ((Integer) ratingsBar.getTag()).intValue();

		// Get the photo
		ImageView imageView = (ImageView) findViewById(R.id.businessPhoto);
		if (imageView != null && imageView.getTag() != null) {
			imagePath = imageView.getTag().toString();
		}

		// TODO- Debug only, remove in prod
		String locStr = "";
		if (thisLocation != null) {
			locStr = String.valueOf(thisLocation.getLatitude()) + ", "
					+ String.valueOf(thisLocation.getLongitude());
		}

		Toast.makeText(
				getApplicationContext(),
				"Location: " + locStr + ", Categories : "

				+ foodCategoriesArray + " , Rating: " + rating
						+ ", PhotoPath: " + imagePath, Toast.LENGTH_LONG)
				.show();
		// TODO - End Debug, remove in prod

		// Error cases validation
		String errorMessage = null;
		if (thisLocation == null) {
			errorMessage = "Unable to determine your location. Please make sure that the location services are enabled.";
		} else if ("".equals(foodCategoriesStr)) {
			errorMessage = "Please specify what you ate or drank";
		} else if (rating == -1) {
			errorMessage = "Business Rating not provided";
		} else if ("".equals(imagePath)) {
			errorMessage = "Business Photo cannot be empty";
		}

		// Build the alert box with validations
		if (errorMessage != null) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Validation Errors!");
			builder.setMessage(errorMessage)
					.setCancelable(false)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
								}
							});
			AlertDialog alert = builder.create();
			alert.show();
		} else {
			try {
				// No errors, post to parse
				ParseObject parseObject = new ParseObject("food");

				// First, the geo
				parseObject.put("location",
						new ParseGeoPoint(thisLocation.getLatitude(),
								thisLocation.getLongitude()));

				// Second the categories
				parseObject.put("categories", foodCategoriesArray);

				// Third the photo
				// parseObject.put("photo", "");

				// Lastly the ratings
				parseObject.put("rating", rating);

				// Save the object
				parseObject.save();

				showAlert("Data saved successfully",
						"Your post was saved successfully. Thanks!");

			} catch (ParseException e) {
				// TODO remove stack traces
				System.out.println("Error " + e.getMessage());
				e.printStackTrace();
				showAlert("Error while saving data",
						"Cannot save data. Please try again");
			}
		}
	}

	private void showAlert(String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(title);
		builder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
					}
				});
		AlertDialog alert = builder.create();
		alert.show();

	}
}
